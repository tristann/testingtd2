const {kata,oche} = require("../helpers");
const {DEFAULT} = require('../constants')

describe("[unit tests]".toLocaleUpperCase(), () => {
    verifyHelloMessage()
    verifyPalindrome()
});

function verifyPalindrome(){
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = null
        test(`oche(word,name) : string - [Tried an invalid word and a valid name] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = {}
        test(`oche(word,name) : string - [Tried an invalid word and a valid name] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = []
        test(`oche(word,name) : string - [Tried an invalid word and a valid name] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = "  "
        test(`oche(word,name) : string - [Tried an invalid word and a valid name] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = ""
        test(`oche(word,name) : string - [Tried an invalid word and a valid name] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = null
        const word = DEFAULT.WORDS.NAME
        test(`oche(word,name) : string - [Tried an invalid nane and a valid word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = {}
        const word = DEFAULT.WORDS.NAME
        test(`oche(word,name) : string - [Tried an invalid nane and a valid word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = []
        const word = DEFAULT.WORDS.NAME
        test(`oche(word,name) : string - [Tried an invalid nane and a valid word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = "  "
        const word = DEFAULT.WORDS.NAME
        test(`oche(word,name) : string - [Tried an invalid nane and a valid word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = ""
        const word = DEFAULT.WORDS.NAME
        test(`oche(word,name) : string - [Tried an invalid nane and a valid word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = null
        const word = null
        test(`oche(word,name) : string - [Tried an invalid nane and word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = {}
        const word = {}
        test(`oche(word,name) : string - [Tried an invalid nane and word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = []
        const word = []
        test(`oche(word,name) : string - [Tried an invalid nane and word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = "  "
        const word = "  "
        test(`oche(word,name) : string - [Tried an invalid nane and word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = ""
        const word = ""
        test(`oche(word,name) : string - [Tried an invalid nane and word] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = "stop!"
        test(`oche(word,name) : string - [Tried an invalid word and a valid name] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('02');
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = "hola"
        test(`oche(word,name) : string - [Verify if a word can be reversed] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.reversed).not.toBeNull();
            expect(ans.reversed).not.toBeUndefined();
            expect(ans.reversed).not.toBeNaN();

            expect(ans.option).toBeNull();

            expect(ans.reversed).toEqual("aloh");
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = 'oto'
        test(`oche(word,name) : string - [Verify if a palindrome can be detected] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.reversed).not.toBeNull();
            expect(ans.reversed).not.toBeUndefined();
            expect(ans.reversed).not.toBeNaN();

            expect(ans.option).not.toBeNull();
            expect(ans.option).not.toBeUndefined();
            expect(ans.option).not.toBeNaN();

            expect(ans.reversed).toEqual(word);
            expect(ans.option).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.GOOD_WORD}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify palindrome${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const word = DEFAULT.WORDS.END_TRIGGER
        test(`oche(word,name) : string - [Verify if the stop word works correctly] - with word = '${word}' and name = '${name}'`, () => {
            const ans = oche(word,name)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();
            
            expect(ans).toEqual(`${DEFAULT.SPANISH_WORDS.CLOSE} ${name}`);
        });
    });
}

function verifyHelloMessage(){
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,10,10,10)
        test(`kata(name,dateTime) : string - [Tried a valid name and a morning value containned between 6 and 12] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.MORNING} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,15,10,10)
        test(`kata(name,dateTime) : string - [Tried a valid name and a afternoon value containned between 12 and 20] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.AFTERNOON} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,21,10,10)
        test(`kata(name,dateTime) : string - [Tried a valid name and a night value containned between 20 and 6] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.NIGHT} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    //BOUNDS
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,6,0,1)
        test(`kata(name,dateTime) : string - [Tried a valid name and a morning value that is the bound of the possible values] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.MORNING} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,12,0,0)
        test(`kata(name,dateTime) : string - [Tried a valid name and a morning value that is the bound of the possible values] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.MORNING} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,12,0,1)
        test(`kata(name,dateTime) : string - [Tried a valid name and a afternoon value that is the bound of the possible values] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.AFTERNOON} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,20,0,0)
        test(`kata(name,dateTime) : string - [Tried a valid name and a afternoon value that is the bound of the possible values] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.AFTERNOON} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,20,0,1)
        test(`kata(name,dateTime) : string - [Tried a valid name and a night value that is the bound of the possible values] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.NIGHT} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried a valid name and a night value that is the bound of the possible values] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans).toEqual(`${DEFAULT.SEPARATORS.WORD_START}${DEFAULT.SPANISH_WORDS.NIGHT} ${name}${DEFAULT.SEPARATORS.WORD_END}`);
        });
    });

    //Bad input
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = null
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = {}
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = []
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = 10
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = new Date()
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = "3### #### ### fdsqfds # ''"
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = ""
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = "01 Jan 1970 00:00:00 GMT"
        const dateTime = new Date(2021,11,30,6,0,0)
        test(`kata(name,dateTime) : string - [Tried an invalid name and a valid date] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = null
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = {}
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = []
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = 10
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = "3### #### ### fdsqfds # ''"
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = ""
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = DEFAULT.WORDS.NAME
        const dateTime = "01 Jan 1970 00:00:00 GMT"
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    // both invalid
    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = null
        const dateTime = null
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = {}
        const dateTime = {}
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = []
        const dateTime = []
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = 10
        const dateTime = 10
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = "3### #### ### fdsqfds # ''"
        const dateTime = "3### #### ### fdsqfds # ''"
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = ""
        const dateTime = ""
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });

    describe(`${DEFAULT.SEPARATORS.TEST_DESCRIBE_START}verify hello message${DEFAULT.SEPARATORS.TEST_DESCRIBE_END}`.toLocaleUpperCase(), () => {
        const name = "01 Jan 1970 00:00:00 GMT"
        const dateTime = "01 Jan 1970 00:00:00 GMT"
        test(`kata(name,dateTime) : string - [Tried an invalid dateTime and a valid name] - with name = '${name}' and dateTime = ${dateTime}`, () => {
            const ans = kata(name,dateTime)

            expect(ans).not.toBeNull();
            expect(ans).not.toBeUndefined();
            expect(ans).not.toBeNaN();

            expect(ans.msg).not.toBeNull();
            expect(ans.msg).not.toBeUndefined();
            expect(ans.msg).not.toBeNaN();

            expect(ans.code).not.toBeNull();
            expect(ans.code).not.toBeUndefined();
            expect(ans.code).not.toBeNaN();

            expect(ans.code).toEqual('01');
        });
    });
}
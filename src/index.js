const readline = require('readline');
const { DEFAULT } = require('./constants');
const { kata, oche } = require("./helpers");
var validator = require('validator');

function promptQuestion(name, rl) {
    rl.question('', (reversedAnswer) => {
        reversedRes = oche(reversedAnswer, name);

        const expected = DEFAULT.SPANISH_WORDS.CLOSE + " " + name;

        if (typeof reversedRes == "string" && validator.equals(reversedRes, expected)) {
            console.log(reversedRes);
            rl.close();
        } else {
            if (typeof reversedRes.reversed != "string") {
                rl.close();
                console.log(`${reversedRes.msg}`);
            } else {
                console.log(`${reversedRes.reversed}`);
                if (typeof reversedRes.option == "string") {
                    console.log(`${reversedRes.option}`);
                }
                promptQuestion(name, rl);
            }
        }
    });
}

function mainConsole() {
    let name = '';
    const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

    // Entry point
    rl.question('', (answer) => {
        const answers = answer.split(' ');

        if (answers.length != 2) {
            return {
                msg: `error`,
                code: "01"
            }
        }

        res = kata(answers[1], new Date());

        if (typeof res != "string") {
            rl.close();
            console.log(`${res.msg}`);
        } else {
            name = answers[1];
            console.log(`${res}`);

            promptQuestion(name, rl);
        }
    });
}

mainConsole();
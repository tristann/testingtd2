const DEFAULT = {
    SPANISH_WORDS: {
        MORNING: "Buenas dias",
        AFTERNOON: "Buenas tardes",
        NIGHT: "Buenas noches",
        GOOD_WORD: "Bonita palabra",
        CLOSE: "Adios"
    },
    SEPARATORS: {
        WORD_START: "¡",
        WORD_END: "!",
        TEST_DESCRIBE_START: "<",
        TEST_DESCRIBE_END: ">"
    },
    WORDS: {
        NAME: "Gon",
        END_TRIGGER: "Stop!"
    }
}

module.exports = {
    DEFAULT
}

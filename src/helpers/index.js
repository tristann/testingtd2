var validator = require('validator');
const { DEFAULT } = require('../constants');

function testDay(date) {

    const milliHours = date.getHours() * 3600 * 1000
    const milliMin = date.getMinutes() * 60 * 1000
    const milliSec = date.getSeconds() * 1000

    return (milliHours + milliMin + milliSec + date.getMilliseconds())
}

function dayTime(date) {
    const HOUR_OF_DAY = testDay(date);

    const HOUR_MORNING = HOUR_OF_DAY > testDay(new Date(2021, 11, 30, 6, 0, 0, 0)) && HOUR_OF_DAY < testDay(new Date(2021, 11, 30, 12, 0, 0, 1));
    const HOUR_AFTERNOON = HOUR_OF_DAY > testDay(new Date(2021, 11, 30, 12, 0, 0, 0)) && HOUR_OF_DAY < testDay(new Date(2021, 11, 30, 20, 0, 0, 1));

    if (HOUR_MORNING) {
        return DEFAULT.SPANISH_WORDS.MORNING;
    } else if (HOUR_AFTERNOON) {
        return DEFAULT.SPANISH_WORDS.AFTERNOON;
    } else {
        return DEFAULT.SPANISH_WORDS.NIGHT;
    }
}

function isDate(dateInput) {
    if (typeof dateInput == "number") {
        return false;
    }

    let state;
    try {
        new Date(dateInput).toISOString();
        state = true;
    } catch (error) {
        state = false;
    } finally {
        return state;
    }
}

function veryExpression(input) {
    let temp = true;

    if (validator.isEmpty(input)) {
        return false;
    }

    if (!validator.isAlpha(input)) {
        return false;
    }

    const excludeFormat = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~1-9¨£§²µ€]/;
    const includeFormat = /[a-zA-Z]/;

    if (excludeFormat.test(input) || !includeFormat.test(input)) {
        return false;
    }

    return temp;
}

function oche(userInput, name) {
    // CHECK USERINPUTS
    if (typeof userInput != "string" || typeof name != "string") {
        return {
            msg: `The input must be a string, you entered a '${typeof userInput}'`,
            code: "01"
        }
    }

    // CHECK EXPRESSION
    if (!veryExpression(name)) {
        return {
            msg: `The input is not well formed and contains spaces, special characters or is empty`,
            code: "02"
        }
    }

    // STOP ?
    if (validator.equals(userInput, DEFAULT.WORDS.END_TRIGGER)) {
        return DEFAULT.SPANISH_WORDS.CLOSE + " " + name
    }

    // CHECK EXPRESSION
    if (!veryExpression(userInput)) {
        return {
            msg: `The input is not well formed and contains spaces, special characters or is empty`,
            code: "02"
        }
    }

    var splitString = userInput.split("");
    var reverseArray = splitString.reverse();
    var res = reverseArray.join("");

    // PERFECT PALINDROM
    if (validator.equals(res, userInput)) {
        return {
            reversed: res,
            option: DEFAULT.SEPARATORS.WORD_START + DEFAULT.SPANISH_WORDS.GOOD_WORD + DEFAULT.SEPARATORS.WORD_END,
        }
    }

    return {
        reversed: res,
        option: null
    }
}

function kata(userInput, date) {

    // CHECK USERINPUT
    if (typeof userInput != "string" && typeof date != "date") {
        return {
            msg: `The input must be a string, you entered a '${typeof userInput}'`,
            code: "01"
        }
    }

    // CHECK EXPRESSION
    if (!veryExpression(userInput)) {
        return {
            msg: `The input is not well formed and contains spaces, special characters or is empty`,
            code: "01"
        }
    }

    // CHECK DATE
    if (!validator.isDate(date)) {
        return {
            msg: `The second input must be a valid date`,
            code: "01"
        }
    }

    return DEFAULT.SEPARATORS.WORD_START + dayTime(date) + " " + userInput + DEFAULT.SEPARATORS.WORD_END;
}

module.exports = {
    oche,
    kata
}